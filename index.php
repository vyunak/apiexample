<?php 

// example

$public_key = '07317b0065ad73297d1df40116b7807286b4244f';
$secret_key = '4b44d74003799f98f507ed29695b0a993244c4cf';

$content_type = 'application/json';

$nonce = time() * 1000;

$uniqid = uniqid();

// data for request
$body = array();

// http link for signature
$body = http_build_query($body);

$apiPath = 'v2/wallet';

// message for signature
$message = '/api/' . $apiPath . $uniqid . $nonce . $body;

// or sha256, sha384, sha512
$SIGNATURE = hash_hmac('sha512', $message, $secret_key);

$headers = array(
    'X-Auth-Signature: ' . $SIGNATURE,
    'X-Auth-Apikey: '. $public_key,
    'X-Auth-Nonce: '. $nonce,
    'X-Auth-Uniqid: '. $uniqid,
    'Content-Type: '. $content_type
);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,            	"https://api.tmp.com/{$apiPath}" );
curl_setopt($ch, CURLOPT_RETURNTRANSFER,	1);

curl_setopt($ch, CURLOPT_POST,           	true );
curl_setopt($ch, CURLOPT_POSTFIELDS,     	$body);
curl_setopt($ch, CURLOPT_HTTPHEADER, 		$headers);

$result = curl_exec($ch);

var_dump($result);

curl_close($ch);